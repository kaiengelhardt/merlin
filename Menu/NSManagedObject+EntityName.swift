import Foundation
import CoreData

extension NSManagedObject {
    
    public class func entityName() -> String {
        let fullClassName: String = NSStringFromClass(object_getClass(self))
        let classNameComponents: [String] = split(fullClassName) {$0 == "."}
        return last(classNameComponents)!
    }
    
    class func className() -> String {
        return NSStringFromClass(object_getClass(self))
    }
    
    convenience init(managedObjectContext: NSManagedObjectContext) {
        let entityDescriptions = managedObjectContext.persistentStoreCoordinator?.managedObjectModel.entities as! [NSEntityDescription]
        var entity: NSEntityDescription!
        for entityDescription in entityDescriptions {
            if entityDescription.managedObjectClassName == self.dynamicType.className() {
                entity = entityDescription
                break
            }
        }
        self.init(entity: entity, insertIntoManagedObjectContext: managedObjectContext)
    }
    
}
