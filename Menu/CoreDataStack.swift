import Foundation
import CoreData

public enum ContextToSave {
    case MainContext
    case PrivateContext
}

public class CoreDataStack {
    
    public let storeURL: NSURL
    public let managedObjectModelURL: NSURL?
    
    public lazy var managedObjectModel: NSManagedObjectModel = {
        if let modelURL = self.managedObjectModelURL {
            return NSManagedObjectModel(contentsOfURL: modelURL)!
        } else {
            let bundle = NSBundle(forClass: CoreDataStack.self)
            return NSManagedObjectModel.mergedModelFromBundles([bundle])!
        }
        } ()
    
    public lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.storeURL
        let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
        var error: NSError?
        if let store = coordinator.addPersistentStoreWithType(NSSQLiteStoreType, configuration: nil, URL: url, options: options, error: &error) {
        } else {
            if let error = error {
                NSLog("Error while adding persistent store:\n\(error.localizedDescription)\n\(error.userInfo)")
            }
        }
        return coordinator
        } ()
    
    public lazy var privateManagedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
        } ()
    
    public lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .MainQueueConcurrencyType)
        managedObjectContext.parentContext = self.privateManagedObjectContext
        return managedObjectContext
        } ()
    
    private var _saveNotificationObserver: NSObjectProtocol!
    
    public convenience init(storeURL: NSURL) {
        self.init(storeURL: storeURL, modelURL: nil)
    }
    
    public init(storeURL: NSURL, modelURL: NSURL?) {
        managedObjectModelURL = modelURL
        self.storeURL = storeURL
        _saveNotificationObserver = NSNotificationCenter.defaultCenter().addObserverForName(NSManagedObjectContextDidSaveNotification, object: managedObjectContext, queue: nil) {[weak self] notification in
            if let strongSelf = self {
                strongSelf.save(.PrivateContext)
            }
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(_saveNotificationObserver)
    }
    
    public func save(contextToSave: ContextToSave) {
        var context: NSManagedObjectContext!
        switch (contextToSave) {
        case .MainContext:
            context = managedObjectContext
        case .PrivateContext:
            context = privateManagedObjectContext
        }
        var error: NSError?
        context.save()
    }
    
}

public extension NSManagedObjectContext {
    
    public func save() {
        var error: NSError?
        let success = save(&error)
        if let error = error where !success {
            NSLog("Error while saving managed object context:\n\(error.localizedDescription)\n\(error.userInfo)")
        }
    }
    
}
