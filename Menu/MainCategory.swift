import Foundation
import CoreData

public class MainCategory: NSManagedObject {

    @NSManaged public var imagePath: String
    @NSManaged public var name: String
    @NSManaged public var orderIndex: NSNumber
    @NSManaged public var subcategories: NSSet

}
