import Foundation
import CoreData

public class Variant: NSManagedObject {

    @NSManaged public var amount: NSNumber?
    @NSManaged public var price: NSNumber
    @NSManaged public var unit: String?
    @NSManaged public var product: Product

}
