import Foundation
import CoreData

public class Subcategory: NSManagedObject {

    @NSManaged public var name: String
    @NSManaged public var orderIndex: NSNumber
    @NSManaged public var productPositions: NSSet
    @NSManaged public var supercategory: MainCategory

}
