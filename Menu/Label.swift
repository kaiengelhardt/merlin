import Foundation
import CoreData

public class Label: NSManagedObject {

    @NSManaged public var number: NSNumber
    @NSManaged public var text: String
    @NSManaged public var products: NSSet

}
