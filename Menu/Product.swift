import Foundation
import CoreData

public class Product: NSManagedObject {

    @NSManaged public var detailText: String?
    @NSManaged public var imagePath: String?
    @NSManaged public var name: String
    @NSManaged public var labels: NSSet
    @NSManaged public var subcategoryPositions: NSSet
    @NSManaged public var variants: NSSet

}
