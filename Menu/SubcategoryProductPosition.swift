import Foundation
import CoreData

public class SubcategoryProductPosition: NSManagedObject {

    @NSManaged public var orderIndex: NSNumber
    @NSManaged public var product: Product
    @NSManaged public var subcategory: Subcategory

}
