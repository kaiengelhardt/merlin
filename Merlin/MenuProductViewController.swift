import UIKit
import Menu
import CoreData

class MenuProductViewController: UITableViewController {

    var coreDataStack: CoreDataStack!
    var category: MainCategory!
    
    lazy var subcategories: [Subcategory] = {
        var subcategories = self.category.subcategories.allObjects as! [Subcategory]
        sort(&subcategories, {
            $0.orderIndex.integerValue < $1.orderIndex.integerValue
        })
        return subcategories
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = category.name
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return subcategories.count
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let subcategory = subcategories[section]
        return subcategory.productPositions.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell


        return cell
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel.textColor = .yellowColor()
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let subcategory = subcategories[section]
        return subcategory.name
    }

}
