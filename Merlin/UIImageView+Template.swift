import UIKit

extension UIImageView {
    
    var imageRenderingMode: UIImageRenderingMode {
        get {
            return self.image?.renderingMode ?? .AlwaysTemplate
        }
        set {
            self.image = self.image?.imageWithRenderingMode(newValue)
        }
    }
    
}
