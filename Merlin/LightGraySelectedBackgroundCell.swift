import UIKit

class LightGraySelectedBackgroundCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        let view = UIView()
        view.backgroundColor = UIColor(white: 0.7, alpha: 1.0)
        selectedBackgroundView = view
    }
    
}
