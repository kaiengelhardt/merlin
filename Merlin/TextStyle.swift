import UIKit

class TextStyle: NSObject {
    
    class func preferredFontForTextStyle(style: String) -> UIFont {
        return UIFont.preferredFontForTextStyle(style)
    }
    
}