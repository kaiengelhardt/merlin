import UIKit
import Menu
import CoreData

class MenuCategoryViewController: UICollectionViewController {
    
    let coreDataStack: CoreDataStack = {
        let documentsDirectory: String = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first as! String
        let storePath = documentsDirectory.stringByAppendingPathComponent("Menu.sqlite")
        let URL = NSURL(fileURLWithPath: storePath)!
        let coreDataStack = CoreDataStack(storeURL: URL)
        return coreDataStack
    }()
    
    lazy var categories: [MainCategory] = {
        let fetchRequest = NSFetchRequest(entityName: MainCategory.entityName())
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "orderIndex", ascending: true)]
        var error: NSError?
        let categories = self.coreDataStack.managedObjectContext.executeFetchRequest(fetchRequest, error: &error) as! [MainCategory]
        return categories
    }()
    
    // MARK: - UICollectionViewDataSource

    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }

    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CategoryCell
        let category = categories[indexPath.row]
        cell.nameLabel.text = category.name
        return cell
    }
    
    // MARK: - Navigation
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "Show Category" {
            if let viewController = segue.destinationViewController as? MenuProductViewController,
            indexPath = collectionView?.indexPathsForSelectedItems().first as? NSIndexPath {
                viewController.coreDataStack = coreDataStack
                viewController.category = categories[indexPath.item]
            }
        }
    }

}
