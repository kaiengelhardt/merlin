import UIKit
import MapKit

class TouchThroughMapView: MKMapView {
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        return false
    }

}
