import UIKit

class ContactCell: LightGraySelectedBackgroundCell {
    
    @IBOutlet var contactTypeLabel: UILabel!
    @IBOutlet var dataLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureWithContactType(contactType: String, data: String) {
        contactTypeLabel.text = contactType
        dataLabel.text = data
    }

}
