import UIKit
import MapKit

class AdressCell: LightGraySelectedBackgroundCell {
    
    @IBOutlet var mapView: MKMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let coordinate = CLLocationCoordinate2D(latitude:
            50.253916, longitude: 8.674545)
        mapView.region = MKCoordinateRegion(center: coordinate, span: MKCoordinateSpan(latitudeDelta: 0.003, longitudeDelta: 0.003))
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotation.title = "Bistro Merlin"
        mapView.addAnnotation(annotation)
        mapView.selectAnnotation(annotation, animated: true)
    }
    
}
