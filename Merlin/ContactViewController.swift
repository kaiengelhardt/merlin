import UIKit
import MessageUI

class ContactViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    private let telephoneNumber = "06007/918921"
    private let emailAddress = "mail@merlin-burgholzhausen.de"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 44
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPath = tableView.indexPathForSelectedRow() {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
            transitionCoordinator()?.notifyWhenInteractionEndsUsingBlock({ (context) -> Void in
                if context.isCancelled() {
                    self.tableView.selectRowAtIndexPath(indexPath, animated: false, scrollPosition: .None)
                }
            })
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        case 1:
            return 2
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCellWithIdentifier("OpenTimeCell\(indexPath.row)", forIndexPath: indexPath) as! UITableViewCell
            return cell
        case 1:
            let cell = tableView.dequeueReusableCellWithIdentifier("ContactCell", forIndexPath: indexPath) as! ContactCell
            switch indexPath.row {
            case 0:
                cell.configureWithContactType("Telefon", data: telephoneNumber)
            case 1:
                cell.configureWithContactType("Mail", data: emailAddress)
            default:
                break
            }
            return cell
        default:
            let cell = tableView.dequeueReusableCellWithIdentifier("AddressCell", forIndexPath: indexPath) as! UITableViewCell
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Öffnungszeiten"
        case 1:
            return "Kontakt"
        case 2:
            return "Adresse"
        default:
            return nil
        }
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (1, 0):
            tableView.deselectRowAtIndexPath(indexPath, animated: true) // viewWillAppear is not called when a call ends :(
            callNumber(telephoneNumber)
        case (1, 1):
            writeMailTo(emailAddress)
        default:
            break
        }
    }
    
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel.textColor = .yellowColor()
    }
    
    private func callNumber(number: String) {
        if let callURL = NSURL(string: "tel:\(number)") {
            UIApplication.sharedApplication().openURL(callURL)
        }
    }
    
    private func writeMailTo(emailAddress: String) {
        let composeViewController = MFMailComposeViewController()
        composeViewController.mailComposeDelegate = self
        composeViewController.setToRecipients([emailAddress])
        presentViewController(composeViewController, animated: true, completion: nil)
    }
    
    // MFMailComposeViewControllerDelegate
    
    func mailComposeController(controller: MFMailComposeViewController!, didFinishWithResult result: MFMailComposeResult, error: NSError!) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
}
