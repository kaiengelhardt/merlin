import UIKit

class DynamicTextLabel: UILabel {
    
    private let textStyles = [UIFontTextStyleBody, UIFontTextStyleCaption1, UIFontTextStyleCaption2, UIFontTextStyleFootnote, UIFontTextStyleHeadline, UIFontTextStyleSubheadline]

    @IBInspectable var textStyle: String = UIFontTextStyleBody {
        didSet {
            if find(textStyles, textStyle) == nil {
                textStyle = UIFontTextStyleBody
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "systemFontSizeDidChange:", name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
    func systemFontSizeDidChange(notification: NSNotification) {
        reconfigureFont()
    }
    
    func reconfigureFont() {
        font = TextStyle.preferredFontForTextStyle(textStyle)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UIContentSizeCategoryDidChangeNotification, object: nil)
    }
    
}
