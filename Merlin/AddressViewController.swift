import UIKit
import MapKit

class AddressViewController: UIViewController {
    
    @IBOutlet var mapView: MKMapView!
    @IBOutlet var openInMapsButton: UIButton!
    
    private let locationOfMerlin = CLLocationCoordinate2D(latitude: 50.253916, longitude: 8.674545)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        openInMapsButton.layer.borderColor = UIColor.yellowColor().CGColor
        openInMapsButton.layer.borderWidth = 2.0
        openInMapsButton.layer.cornerRadius = 8.0
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if isBeingPresented() || isMovingToParentViewController() {
            UIView.animateWithDuration(1.0, animations: { () -> Void in
                self.mapView.region = MKCoordinateRegion(center: self.locationOfMerlin, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
            })
            let annotation = MKPointAnnotation()
            annotation.coordinate = locationOfMerlin
            annotation.title = "Bistro Merlin"
            mapView.addAnnotation(annotation)
            mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    @IBAction func openInMaps() {
        let placemark = MKPlacemark(coordinate: locationOfMerlin, addressDictionary: nil)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Bistro Merlin"
        mapItem.openInMapsWithLaunchOptions(nil)
    }

}
